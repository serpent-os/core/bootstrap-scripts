#!/bin/true
set -e

. $(dirname $(realpath -s $0))/common.sh

extractSource gettext
cd gettext-*

printInfo "Configuring gettext"
 ./configure \
    --prefix=/usr \
    --sysconfdir=/etc \
    --libdir=/usr/lib \
    --bindir=/usr/bin \
    --sbindir=/usr/sbin \
    --datadir=/usr/share \
    --target="${SERPENT_TRIPLET}" \
    --host="${SERPENT_HOST}" 

printInfo "Building gettext"
make

printInfo "Installing gettext"
make install DESTDIR="${SERPENT_INSTALL_DIR}"
